<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    //
    public function index(Request $request){
        $sender_id = $request["sender_id"];
        $conversation_id = $request["conversation_id"];
        $receiver_id = \App\Conversation::find($conversation_id)->receiver_id;

        return response()->json(\App\User::find($receiver_id), 200);
    }
}
