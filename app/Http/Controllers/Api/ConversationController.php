<?php

namespace App\Http\Controllers\Api;

use App\Conversation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConversationController extends Controller
{
    //
    public function index(Request $request){
        $conversations = $this->conversations($request["user_id"]);
        return response()->json($conversations, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function conversations($receiver_id)
    {
        //
        $conversations = Conversation::where("receiver_id", $receiver_id)
                                    ->orWhere("sender_id", $receiver_id)->with("sender", "receiver")
                                    ->get();
        return $conversations;
    }
}
