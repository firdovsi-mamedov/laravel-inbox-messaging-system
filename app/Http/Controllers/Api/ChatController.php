<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Chat;
use App\ChatConversation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChatController extends Controller
{

    public function index(Request $request){
        //
        if($request["conversation_id"] !== null){
            $messages = $this->messages( $request["conversation_id"]);
            return response()->json($messages);
        }
        return response()->json($receiver_id);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function messages($conversation_id)
    {
        //
        $messages = \App\Chat::where("conversation_id", $conversation_id)->with("receiver", "sender")->get();
        return $messages;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //
        $messages = \App\Chat::create($request->all());
        $message = \App\Chat::where("id", $messages->id)->with("receiver", "sender")->first();
        return response()->json($message, 200);
    }

    public function done(Request $request){
        $conversation = \App\ChatConversation::find($request["conversation_id"]);
        $conversation->status = 1;
        $conversation->save();

        $user = \App\User::find($request["sender_id"]);

        $message = new \App\Chat();
        $message->conversation_id = $request["conversation_id"];
        $message->sender_id = $request["sender_id"];
        $message->receiver_id = $request["receiver_id"];
        $message->is_admin = 1;
        $message->message = "Исполнитель {$user->name} сообщил о выполнении заказа. Заказчик, проверьте выполнение в течение 24 часов и подтвержите это.";
        $message->save();

        $data = \App\Chat::where("id", $message->id)->with("receiver", "sender")->first();

        return response()->json($data, 200);
    }

    public function accept(Request $request){
        $conversation = \App\ChatConversation::find($request["conversation_id"]);
        $conversation->status = 3;
        $conversation->save();

        $user = \App\User::find($request["sender_id"]);

        $message = new \App\Chat();
        $message->conversation_id = $request["conversation_id"];
        $message->sender_id = $request["sender_id"];
        $message->receiver_id = $request["receiver_id"];
        $message->is_admin = 1;
        $message->message = "Заказчик {$user->name} принял заказ. Благодарим за пользование сервисом.";
        $message->save();

        $data = \App\Chat::where("id", $message->id)->with("receiver", "sender")->first();

        return response()->json($data, 200);
    }


    public function arbitrach(Request $request){

        $conversation = \App\ChatConversation::find($request["conversation_id"]);
        $conversation->status = 2;
        $conversation->save();

        $user = \App\User::find($request["sender_id"]);

        $message = new \App\Chat();
        $message->conversation_id = $request["conversation_id"];
        $message->sender_id = $request["sender_id"];
        $message->receiver_id = $request["receiver_id"];
        $message->is_admin = 1;

        if($user->role == "booster"){
            $message->message = "Заказчик {$user->name} открыл спор. Пожалуйста, как можно скорее решите проблему.";
        }else{
            $message->message = "Исполнитель {$user->name} открыл спор. Пожалуйста, как можно скорее решите проблему.";
        }
        $message->save();

        $data = \App\Chat::where("id", $message->id)->with("receiver", "sender")->first();

        return response()->json($data, 200);
    }

    public function closed(Request $request){

        $conversation = \App\ChatConversation::find($request["conversation_id"]);
        $conversation->status = 4;
        $conversation->save();


        $message = new \App\Chat();
        $message->conversation_id = $request["conversation_id"];
        $message->sender_id = $request["sender_id"];
        $message->receiver_id = $request["receiver_id"];
        $message->is_admin = 1;
        $message->message = "Заказ закрыт администрацией.";
        $message->save();

        $data = \App\Chat::where("id", $message->id)->with("receiver", "sender")->first();

        return response()->json($data, 200);
    }
}
