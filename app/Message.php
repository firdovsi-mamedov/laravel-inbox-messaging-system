<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //
    protected $fillable = ["conversation_id", "sender_id", "receiver_id", "message", "status"];

    //
    public function receiver(){
        return $this->hasOne("App\User", "id", "receiver_id");
    }

    //
    public function sender(){
        return $this->hasOne("App\User", "id", "sender_id");
    }
}
